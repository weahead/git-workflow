# Workflow when repository host is [Github](https://github.com/)

## Rules

- Use branching model [Gitflow](http://nvie.com/posts/a-successful-git-branching-model/)
- Use CLI helper tool [gitflow-avh](https://github.com/petervanderdoes/gitflow-avh)
- Clone origin, create feature branches and push back to origin.
- Ease of day-to-day work for developers is priority one.
- Ease of day-to-day work for maintainers is priority two.
- Create a pull request for each feature branch.
- Do not rebase public commits.
- Do rebase private commits if it helps to improve history.
- Avoid commit messages with "WIP". A feature branch is by default
  considered work in progress until it has been merged.
- Dependant feature branches are discouraged.
  - If needed: create a new feature branch from an existing feature branch
    you need to depend on. A pull request should not be created until all base
    feature branches have been merged to `develop` branch. This is to
    avoid commits from base branches from showing up in the pull
    request. Making the review process very hard and cumbersome.
    Creating a pull request before all base branches have been merged
    usually requires a rebase to clean up the pull request for review, and
    we want to avoid rebase.
- For projects that are our own and that benefits from a changelog should keep a
  changelog following the recommendations at http://keepachangelog.com/.
- Releases follow recommendations at http://semver.org/.


## Examples

### Initialize a repository clone

```sh
# Clone the repository
git clone git@github.com:weahead/repo.git
cd repo

# Initialize gitflow-avh
git flow init
# Usually default answers are correct, except for "production releases" and "version tag prefix".
# Answer the following:
# Branch name for production releases: master
# Branch name for "next release" development: develop
# Feature branches: feature/
# Bugfix branches: bugfix/
# Release branches: release/
# Hotfix branches: hotfix/
# Support branches: support/
# Version tag prefix: v
# Hooks and filters directory? [</path/to/repo>/git-workflow/.git/hooks]
```


### New isolated feature

```sh
# Create a new feature branch named "<feature name>" based on "develop".
# Name it something descriptively.
git flow feature start <feature name>

# Do work
git commit ...

# Optionally, rebase to clean up private commit history.
# Never rebase commits that are public.
# "<base>" is the branch this feature branch was based on, usually "develop"
git rebase -i <base>

# Push to Github
git flow feature publish <feature name>

# Create a new pull request for <feature name> from the web ui.

# Wait for review and merge by someone else.

# Get the latest changes from origin and merge them locally.
git checkout develop
git pull -p

# Remove local tracking branch.
git flow feature delete <feature name>
```


### New feature that depends on other unmerged feature

```sh
# Create new feature branch named "<feature name>" based on "<dependency name>".
# Name it something descriptively.
git flow feature start <feature name> <dependency name>

# Do work
git commit ...

# Optionally, rebase to clean up history.
# "<base>" is the branch this feature branch was based on, usually "<dependency name>"
git rebase -i <base>

# Push to Github
git flow feature publish <feature name>

# Wait for the merge request for "<dependency name>" to get merged.

# Pro tip: subscribe to the merge request for "<dependency name>"
#          to get notified of when it gets merged.

# Create a new pull request for "<feature name>" from the web ui.

# Wait for review and merge by someone else.

# Get the latest changes from origin and merge them locally.
git checkout develop
git pull -p

# Verify that your feature branch was properly merged in the pull.
# If your feature branch is listed in this output, it was properly merged.
git branch --merged

# Remove local tracking branch.
git flow feature delete -f <feature name>
```
