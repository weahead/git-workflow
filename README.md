# We ahead's branching model and workflow for internal git-based projects

## About

This repository documents We ahead's workflow for git-based projects.

It is split up into different rules for different repository hosts.
Mostly due to the fact that they all behave a little different from
each other, so we have to alter the workflow a little here and there
to accomodate those differences.


## Repository hosts

- [Github](github.md)
- [Gitlab](gitlab.md)


## License

[ARR](LICENSE)

